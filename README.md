# IT Minds Infoscreen Widget Configurations

This is an [IT Minds](https://it-minds.dk/) project for the info dashboard widget configurations.

## Getting Started

Create your widget configuration file

```sh
touch widgets/[MY_WIDGETNAME].json
```

Proceed by using the built in json schema by assind this to the file

```json
{
  "$schema": "../schema.json"
}
```

Fill out the configuration file.

## Deploying

Commit your file to a branch and push.

```bash
git checkout feature/[MY_WIDGETNAME]
git add widgets/[MY_WIDGETNAME].json
git commit -m "Configuration file for [MY WIDGETNAME]"
git push -u git@bitbucket.org:itmindsdk/itm-infoscreen-dashboard-widgets.git feature/[MY_WIDGETNAME]
```

On BitBucket [create a pull request](https://bitbucket.org/itmindsdk/itm-infoscreen-dashboard-widgets/pull-requests)
