import {
  Matches,
  validateOrReject,
  Length,
  Min,
  Max,
  ValidateNested,
  IsArray,
  IsOptional,
  ArrayNotEmpty,
  IsInt,
  IsUrl,
  IsString
} from "class-validator";

import {
  JSONSchema,
  validationMetadatasToSchemas
} from "class-validator-jsonschema";

class Size {
  @Min(1)
  @Max(4)
  @IsInt()
  width: number;

  @Min(1)
  @Max(4)
  @IsInt()
  height: number;

  @Min(1)
  @Max(9)
  @IsOptional()
  get area() {
    return this.width * this.height;
  }
}

class Attributes {
  @IsString()
  name: string;

  @IsString()
  description: string;
}

@JSONSchema({
  description: "A widget for the IT Minds info dashboard",
  example: {
    id: "bitbucket-commits",
    owners: ["mjh@it-minds.dk"],
    times: {
      prep: 10,
      shown: 20
    }
  }
})
export default class Schema {
  @Length(5, 20)
  id: string;

  @IsOptional()
  @IsString()
  description: string;

  @IsArray()
  @Matches(/^\w+@it-minds\.\w{2,4}$/, {
    each: true
  })
  owners: string[];

  @IsArray()
  @ArrayNotEmpty()
  @ValidateNested({ each: true })
  sizes: Size[];

  @IsOptional()
  @IsUrl()
  url?: string;

  @IsOptional()
  @IsArray()
  @ValidateNested()
  attributes?: Attributes[];

  static validate = (obj: Partial<Schema>) => {
    const self = Object.assign(new Schema(), obj);
    self.sizes = obj.sizes?.map(size => Object.assign(new Size(), size)) ?? [];

    if (obj.attributes) {
      self.attributes = obj.attributes.map(a =>
        Object.assign(new Attributes(), a)
      );
    } else {
      self.attributes = [];
    }

    return validateOrReject(self, {
      forbidNonWhitelisted: true
    });
  };
}

export const genSchema = () => validationMetadatasToSchemas();
