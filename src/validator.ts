import path from "path";
import fs from "fs";
import Schema from "./Schema";

const widgetsPath = path.resolve(__dirname, "..", "widgets");

const pattern = /^[\w_-]+\.json$/;
let errors = 0;
Promise.all(
  fs.readdirSync(widgetsPath).map(async widgetPath => {
    if (!pattern.test(widgetPath)) return;

    const FQP = path.join(widgetsPath, widgetPath);

    const json = JSON.parse(fs.readFileSync(FQP).toString());
    if (json.$schema) delete json.$schema;

    try {
      await Schema.validate(json);
      console.info("Validation complete: [OK]", widgetPath);
    } catch (err) {
      console.error("Validation complete: [ERROR]", widgetPath);
      console.debug(JSON.stringify(err, null, 2));
      errors++;
    }
  })
)
  .then(() => {
    if (errors > 0) {
      console.info("Some schemas aren't valid, check above for errors!");
      process.exit(1);
    } else {
      console.info("All schemas are valid!");
      process.exit(0);
    }
  })
  .catch(err => {
    console.error("Something went wrong during pre validation of a schema");
    console.error(err);
    process.exit(1);
  });
