import { writeFileSync } from "fs";
import { resolve } from "path";
import { genSchema } from "./Schema";

const finalSchema = {
  $schema: "http://json-schema.org/draft-07/schema#",
  $id:
    "https://bitbucket.org/itmindsdk/itm-infoscreen-dashboard-widgets/src/schema.json",
  definitions: genSchema(),
  $ref: "#/definitions/Schema"
};

const finalSchemaString = JSON.stringify(finalSchema, null, 2);

writeFileSync(resolve("..", "schema.json"), finalSchemaString + "\n");
