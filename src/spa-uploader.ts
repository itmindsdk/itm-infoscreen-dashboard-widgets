import { BlobServiceClient, ContainerClient } from "@azure/storage-blob";
import { existsSync, lstatSync, readdirSync, readFileSync } from "fs";
import path from "path";
import Schema from "./Schema";

import mime from "mime-types";

const uploadBlobToContainer = async (
  containerName: string,
  filepath: string,
  widgetId: string,
  connectionString: string
) => {
  filepath = path.resolve(filepath);
  filepath = filepath.replace(/\\/g, "/");

  const widgetConfigPath = path.resolve(`../widgets/${widgetId}.json`);
  //Check if config exists:
  if (!existsSync(widgetConfigPath)) {
    console.error(`Widget config file '${widgetId}.json' does not exist.`);
    return;
  }

  //Check if config is valid:
  const json = JSON.parse(readFileSync(widgetConfigPath).toString());
  if (json.$schema) delete json.$schema;
  try {
    await Schema.validate(json);
    console.info("Validation complete: [OK]", widgetId);
  } catch (err) {
    console.error("Validation complete: [ERROR]", widgetId);
    return;
  }

  //Check if upload has an index.html file:
  if (!existsSync(filepath + "/index.html")) {
    console.error(
      "Index.html does not exist. Make sure your widget is correctly exported as static HTML."
    );
    return;
  }

  const blobServiceClient = BlobServiceClient.fromConnectionString(
    connectionString
  );
  const containerClient = blobServiceClient.getContainerClient(containerName);
  _upload(filepath, containerClient, widgetId);
};

const _upload = (
  filepath: string,
  containerClient: ContainerClient,
  subfolder: string
) => {
  readdirSync(filepath).forEach(file => {
    const completePath = filepath + "/" + file;
    //run recursively if file is a folder
    if (lstatSync(completePath).isDirectory()) {
      const tempFolder = subfolder + "/" + file;
      _upload(completePath, containerClient, tempFolder);
      return;
    }
    const blockBlobClient = containerClient.getBlockBlobClient(
      subfolder + "/" + file
    );
    console.info("\nUploading to Azure storage as blob:\n\t", completePath);
    try {
      const mimeType = mime.lookup(completePath);

      blockBlobClient.uploadFile(completePath, {
        blobHTTPHeaders: {
          blobContentType: mimeType
        }
      });
    } catch (error) {
      console.error(error);
    }
  });
};

const prompt = require("prompt");

const schema = {
  properties: {
    blobContainer: {
      description: "Enter the name of the blob container",
      type: "string",
      default: "$web",
      required: true
    },
    path: {
      description:
        "Enter the path of the widget's static HTML export out folder",
      type: "string",
      required: true
    },
    widgetId: {
      description: "Enter the name/id of the widget",
      type: "string",
      required: true
    },
    connectionString: {
      description: "Enter the connection string to the blob storage",
      type: "string",
      required: true
    }
  }
};

prompt.start();

prompt.get(schema, (err, result) => {
  if (err) {
    return onErr(err);
  }

  uploadBlobToContainer(
    result.blobContainer,
    result.path,
    result.widgetId,
    result.connectionString
  );
});

const onErr = err => {
  console.error(err);
  return 1;
};
